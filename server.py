import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.gen
import tornado.autoreload
from tornado.options import define, options
import os
import time
import multiprocessing
#import serialworker
import json
import sys
import serial
import re

GRBL_ERRORS={0:'STATUS_OK',
1:'STATUS_EXPECTED_COMMAND_LETTER',
2:'STATUS_BAD_NUMBER_FORMAT',
3:'STATUS_INVALID_STATEMENT',
4:'STATUS_NEGATIVE_VALUE',
5:'STATUS_SETTING_DISABLED',
6:'STATUS_SETTING_STEP_PULSE_MIN',
7:'STATUS_SETTING_READ_FAIL',
8:'STATUS_IDLE_ERROR',
9:'STATUS_ALARM_LOCK',
10:'STATUS_SOFT_LIMIT_ERROR',
11:'STATUS_OVERFLOW',
12:'STATUS_MAX_STEP_RATE_EXCEEDED',
15:'JOG_SOFT_LIMIT',
20:'STATUS_GCODE_UNSUPPORTED_COMMAND',
21:'STATUS_GCODE_MODAL_GROUP_VIOLATION',
22:'STATUS_GCODE_UNDEFINED_FEED_RATE',
23:'STATUS_GCODE_COMMAND_VALUE_NOT_INTEGER',
24:'STATUS_GCODE_AXIS_COMMAND_CONFLICT',
25:'STATUS_GCODE_WORD_REPEATED',
26:'STATUS_GCODE_NO_AXIS_WORDS',
27:'STATUS_GCODE_INVALID_LINE_NUMBER',
28:'STATUS_GCODE_VALUE_WORD_MISSING',
29:'STATUS_GCODE_UNSUPPORTED_COORD_SYS',
30:'STATUS_GCODE_G53_INVALID_MOTION_MODE',
31:'STATUS_GCODE_AXIS_WORDS_EXIST',
32:'STATUS_GCODE_NO_AXIS_WORDS_IN_PLANE',
33:'STATUS_GCODE_INVALID_TARGET',
34:'STATUS_GCODE_ARC_RADIUS_ERROR',
35:'STATUS_GCODE_NO_OFFSETS_IN_PLANE',
36:'STATUS_GCODE_UNUSED_WORDS',
37:'STATUS_GCODE_G43_DYNAMIC_AXIS_ERROR'}

realtimeCommands=['\x18','\x85','?','~','!']
 
class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('static/index.html')

class StaticFileHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('static/main.js')


class MyWebSocketHandler(tornado.websocket.WebSocketHandler):
    def check_origin(self, origin):
        return True
    def initialize(self,parent, sp,clients):
        self.parent = parent
        self.sp = sp
        self.clients=clients
        self.enableStatus=False
        self.enableStatusNext=False
    def open(self):
        print('new connection')
        self.clients.append(self)
        self.write_message("ConnectionOK")
 
    def on_message(self, message):
        #print('tornado received from client: %s' % str(message))
        try:
            cmd=json.loads(message)
            cmd['client']=self
            self.parent.sendCommand(cmd)
        except ValueError:
            self.write_message(json.dumps({'error':'comand not in JSON format','cmd':message}))
            
        
    def on_close(self):
        print('connection closed')
        self.clients.remove(self)

def decodeGRBLfeedback(line):
    if line[0]=='{' and line[-1]=='}':
        return json.loads(line)
    elif line[0]=='<' and line[-1]=='>':
        params=line[1:-1].split('|')
        jsonRet={'State':params[0]}
        for par in params[1:]:
            if par.startswith("MPos:"):
                jsonRet["MPos"]=[float(x) for x in par[5:].split(',')]
            if par.startswith("FS:"):
                jsonRet["FS"]=[float(x) for x in par[3:].split(',')]
            if par.startswith("WCO:"):
                jsonRet["WCO"]=[float(x) for x in par[4:].split(',')]
            if par.startswith("OV:"):
                jsonRet["OV"]=[float(x) for x in par[3:].split(',')]
        return jsonRet

class GrblServer():
    def __init__(self, serialPortName,baudrate=115200,port=5020,address='',url=''):
        self.sp = serial.Serial(serialPortName, baudrate, timeout=0.1)
        self.clients = [] 
        self.commandList=[]
        self.grblState='undef'
        self.readySend=True
        self.lastCmd=None
        self.setReadyCnt=0
        app = tornado.web.Application(
            handlers=[
                    (r"%s/"%url, IndexHandler),
                    (r"%s/static/(.*)"%url, tornado.web.StaticFileHandler, {'path':  './static/'}),
                    (r"%s/ws"%url, MyWebSocketHandler,dict(sp=self.sp,parent=self,clients=self.clients))
                ],debug=True)
        httpServer = tornado.httpserver.HTTPServer(app)
        httpServer.listen(port, address=address)
        print("Listening on port:", port)
        tornado.autoreload.start()
        for dir, _, files in os.walk('static'):
            [tornado.autoreload.watch(dir + '/' + f) for f in files if not f.startswith('.')]

        ## adjust the scheduler_interval according to the frames sent by the serial port
        scheduler_interval = 200
        io_loop = tornado.ioloop.IOLoop.current()
        scheduler = tornado.ioloop.PeriodicCallback(self.actualizeStatus, scheduler_interval)
        scheduler.start()
        io_loop.start()
        
    def sendCommand(self,command):
        if 'data' in command:
            if command['data'] in realtimeCommands:
                self.sendRtCommand(command)
            else:
                self.commandList.append(command)
                self.checkQueue()
        if 'srvCmd' in command:
            if command['srvCmd']=='enableStatus':
                command['client'].enableStatus=True
            if command['srvCmd']=='enableStatusNext':
                command['client'].enableStatusNext=True 
        
        
    def sendRtCommand(self,command):
        self.sp.write(command['data'].encode('utf-8'))

        
    def actualizeStatus(self):
        self.sp.write(b"?")
        self.checkQueue(waitRcv=True)
        
    def writeToAll(self, message):
        for c in self.clients:
            if c.enableStatus or c.enableStatusNext:
                c.write_message(message)
                c.enableStatusNext=False
    def checkQueue(self, waitRcv=False):
        if self.readySend :
            try:
                cmd=self.commandList.pop(0)
                self.sp.write((cmd['data']+'\n').encode('utf-8'))
                self.lastCmd=cmd
                self.readySend=False
                
            except IndexError as e:
                pass
                #print('no command to send')
        while self.sp.in_waiting>0 or not self.readySend or waitRcv:
            line=self.sp.readline().decode('utf-8').replace('\r\n','')
            
            
            #print("rcv: "+ line)
            if line=="":
                print("checkQueue Timeout readySend:"+str(self.readySend)+" waitRcv:"+str(waitRcv))
                break;
            GRBLfeedback=decodeGRBLfeedback(line)
             
            if GRBLfeedback:
                #print(self.readySend)
                #print(json.loads(line)['State'])
                if not self.readySend and GRBLfeedback['State'] in ['Idle','Alarm']:
                    self.setReadyCnt+=1
                    if self.setReadyCnt>10 :
                        self.readySend=True
                        print('Set send ready to one')
                        self.setReadyCnt=0
                else:
                    self.setReadyCnt=0
                self.writeToAll(json.dumps(GRBLfeedback))
            elif line=='ok':
                self.readySend=True
                try:
                    if self.lastCmd['feedback']:
                        self.lastCmd['client'].write_message(line)
                except:
                    pass
                self.lastCmd=None
                self.setReadyCnt=0
                #print('ok')
                self.checkQueue()
                
            elif line.startswith( 'Grbl' ):
                self.writeToAll(line)

            elif line.startswith( 'error' ):
                try:
                    errorMsg='Error: '+GRBL_ERRORS[int(re.match('error:(\d+)',line).group(1))]
                except:
                    errorMsg=line
                print(errorMsg)

                try:
                    self.lastCmd['client'].write_message(errorMsg)
                    print(errorMsg)
                except:
                    print('write_message error in error')
                
                self.readySend=True
                self.lastCmd=None
                
            else:
                print("Recieved: "+ line)
                try:
                    if self.lastCmd['feedback']:
                        self.lastCmd['client'].write_message(line)
                except:
                    print('write_message error in Recieved', sys.exc_info()[0])
            waitRcv=False


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='GRBL_Server')

    parser.add_argument('serialPortName',
                       help='Serial port name (ex: /dev/ttyUSB0)')
    parser.add_argument('-b, --baudrate',dest='baudrate', type=int, default=115200,
                       help='Serial port baudrate (default= 115200)')
    parser.add_argument('-p, --port',dest='port', type=int, default=5020,
                       help='Server port number (default=5020)')
    parser.add_argument('-a, --address',dest='address', default='',
                       help='allowed host (default='')')
    parser.add_argument('-u, --url',dest='url', default='',
                       help='base url (default='', example:"/stepcraft")')
                       
    args = vars(parser.parse_args())

    print(args)
    grbl=GrblServer(**args)
