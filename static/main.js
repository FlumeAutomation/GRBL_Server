$(document).ready(function(){

var received = $('#received');



if (window.location.protocol==='https:'){
    var socketAdress="wss://"+window.location.host+window.location.pathname+"ws";
}
else{
    var socketAdress="ws://"+window.location.host+window.location.pathname+"ws";
}

window.location.href

console.log("socketAdress: "+ socketAdress);

var socket = new WebSocket(socketAdress)
 
socket.onopen = function(){  
  console.log("connected "+socketAdress);
  sendMessage({ 'srvCmd' : 'enableStatus'});
}; 

function pad(num, size) {
    var s = "        " + num.toFixed(3);
    return s.substr(s.length-size);
}

socket.onmessage = function (message) {
  	//console.log("receiving: " + message);
	try {
  		data=JSON.parse(message.data)
	  	document.getElementById("position").innerHTML =  '<pre> '+data.State+' <br/> X'+pad(data.MPos[0],8)+'    Y'+pad(data.MPos[1],8)+'    Z'+pad(data.MPos[2],8)+'</pre>';
	}
	catch(e) {
    	received.append(message.data);
  		received.append($('<br/>'));
	}
};

socket.onclose = function(){
  console.log("disconnected"); 
};

var sendMessage = function(message) {
  console.log("sending:" + JSON.stringify(message));
  socket.send(JSON.stringify(message));
};


function MouseWheelHandler(e) {

	// cross-browser wheel delta
	var e = window.event || e; // old IE support
	var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));

	console.log(e.detail)
	axis=document.querySelector('input[name = "axisSel"]:checked').value;

	sendMessage({ 'data' : '$J=G91'+axis+document.getElementById('step').value*delta+'F1000'})
	e.preventDefault();
};
// GUI Stuff


var jog_btn = document.getElementById("jog_btn");
if (jog_btn.addEventListener) {
	// IE9, Chrome, Safari, Opera
	jog_btn.addEventListener("mousewheel", MouseWheelHandler, false);
	// Firefox
	jog_btn.addEventListener("DOMMouseScroll", MouseWheelHandler, false);
}



$("#homing_btn").click(function(ev){
  console.log("Homing");
  sendMessage({ 'data' : '$H','feedback':true});
});

$("#pause_btn").click(function(ev){
  console.log("Pause");
  sendMessage({ 'data' : '!','feedback':true});
});

$("#resume_btn").click(function(ev){
  console.log("Resume");
  sendMessage({ 'data' : '~','feedback':true});
});

$("#killAlarm_btn").click(function(ev){
  console.log("Kill alarm $X");
  sendMessage({ 'data' : '$X','feedback':true});
});

$("#reset_btn").click(function(ev){
  console.log("Reset");
  sendMessage({ 'data' : String.fromCharCode(0x18),'feedback':true});
});

$("#jog_btn").click(function(ev){
  console.log("Jog cancel");
  sendMessage({ 'data' : String.fromCharCode(0x85),'feedback':true});
});


// send a command to the serial port
$("#cmd_send").click(function(ev){
  ev.preventDefault();
  var cmd = $('#cmd_value').val();
  sendMessage({ 'data' : cmd,'feedback':true});
  $('#cmd_value').val("");
});

$('#clear').click(function(){
  received.empty();
});


});
