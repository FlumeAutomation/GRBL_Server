GRBL_Server is a Tornado based server that allows remote control of a GRBL based CNC controller.
It listens to commands from TCP/IP and transfers them through the serial connection to the Arduino.
It also provides a GUI that can be opened with a Web-browser.

With http requests it is also possible to control the CNC without GUI. This can be useful for automated navigation.

Installation

- Install GRBL on the arduino board 
  follow the instructions on this website: https://github.com/gnea/grbl/wiki/Compiling-Grbl

- Install Python interpreter version 3  

- Get the GRBL source code 
  ```console
  git clone git@gitlab.com:FlumeAutomation/GRBL_Server.git
  ```
  Or download the ZIP file and unzip it
  
- open a terminal and navigate to the directory
  ```console
  cd GRBL_Server
  ```
  
- install the packages
  ```console
  pip3 install -r requirements.txt
  ```



- run the server
  
  For Linux if the serial port is ttyUSB0
  ```console
  python3 server.py /dev/ttyUSB0 
  ```
  For Windows if the arduino is connected on COM4
  ```console
  python3 server.py COM4
  ```
  You can list all the options with this command:
    ```console
  python3 server.py --help
  ```
  
- It is now possible to control the GRBL from a web browser 
  http://localhost:5020/
  
Controlling from python

- the library is located in the "lib" folder

```python
import GRBLserver
```
